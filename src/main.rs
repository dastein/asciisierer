use std::env;
use std::fs::File;
use std::io::{Write, BufReader, BufRead};

fn main() {
    let datname = "/home/imperator/testdatei.txt";
    let rausname = "/home/imperator/asciisiert.txt";
    let mut rein = String::new();
    let mut raus = String::new();

    println!("Zu öffnende Datei: {}", datname);

    let mut reinfile = File::open(datname).expect("Datei nicht gefunden!");
    let mut rausfile = File::create(rausname);
    reinfile.read_to_string(&mut rein);

    for x in rein.chars() {
        match x {
            'ü' => { raus.push_str("ue"); },
            'ö' => { raus.push_str("oe"); },
            'ä' => { raus.push_str("ae"); },
            'ß' => { raus.push_str("ss"); },
            'Ü' => { raus.push_str("UE"); },
            'Ö' => { raus.push_str("OE"); },
            'Ä' => { raus.push_str("AE"); },
            'ẞ' => { raus.push_str("SS"); },
            _ => { raus.push(x); }
        }
    }

//    println!("{}", raus);

    rausfile.write_string(raus);
}
